#language: es
@wip
Característica: Presupuestación de casamiento

Antecedentes:
  Dado que mi cuit personal es "20445556667"

  Escenario: c1 - Casamiento discreto con menu mixto
    Dado un evento "casamiento" con servicio "normal"
    Y que está programado para el "2021-05-01" que es un día no hábil
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 2475.0
    #COSTO_BASE (150) * CANTIDAD_DE_COMENZALES(10) + CANTIDAD_DE_MOZOS(1) * COSTO_MOZO (200) + MENU (55) * CANTIDAD_DE_COMENZALES(10) + COSTO_SALON (si es contratado)


  Escenario: c2 - Casamiento superior con tres menus
    Dado un evento "casamiento" con servicio "superior"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Y que tendrá 50 comensales con menú "veggie"
    Y que tendrá 50 comensales con menú "carnie"
    Cuando se presupuesta
    Entonces el importe resultante es 31950.0

  Escenario: c3 - Casamiento superior con un menu mixto para 300 personas
    Dado un evento "casamiento" con servicio "superior"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 300 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 63900.0
    #COSTO_BASE (150) * CANTIDAD_DE_COMENZALES(300) + CANTIDAD_DE_MOZOS(12) * COSTO_MOZO (200) + MENU (55) * CANTIDAD_DE_COMENZALES(300)
    # 45000 + 2400 + 16500

  Escenario: c4 - Casamiento superior con un menu mixto
    Dado un evento "casamiento" con servicio "superior"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 1 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 405.0

  Escenario: c5 - Casamiento superior con un menu superior para una persona en un salon
    Dado un evento "casamiento" con servicio "superior"
    Y contrata un salon
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 1 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 1405.0

  Escenario: c6 - Casamiento veggie
    Dado un evento "casamiento" con servicio "normal"
    Y que está programado para el "2021-05-07" que es un día hábil
    Y que tendrá 230 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 47600.0
    #COSTO_BASE (150) * CANTIDAD_DE_COMENZALES(230) + CANTIDAD_DE_MOZOS(8) * COSTO_MOZO (200) + MENU (50) * CANTIDAD_DE_COMENZALES(230) + COSTO_SALON (si es contratado)
    #  34500 + 1600 + 11500

  Escenario: c7 - Casamiento veggie en fin de semana
    Dado un evento "casamiento" con servicio "normal"
    Y que está programado para el "2021-05-08" que es un día no hábil
    Y que tendrá 230 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 52360.0
