#language: es
@wip
Característica: Graduacion

  Escenario: g1 - Evento de graduacion con menos de 20 personas no está permitido
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 19 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: g2 - Evento de graduacion no está permitido ser contratado por empresas
    Dado que mi cuit personal es "30445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error

  Escenario: g3 - Evento de graduacion con mas de 60 personas no está permitido
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 61 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: g4 - Fiesta de gradaucion con 40 menu veggie
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 40 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 10200.0

  Escenario: g5 - Fiesta de gradaucion con 40 personas un dia no habil
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Y que está programado para el "2021-05-16" que es un día no hábil
    Y que tendrá 40 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces el importe resultante es 11220.0

  Escenario: g6 - Fiesta de gradaucion con 10 menus de cada uno
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y contrata un salon
    Y que está programado para el "2021-05-16" que es un día no hábil
    Y que tendrá 10 comensales con menú "veggie"
    Y que tendrá 10 comensales con menú "carnie"
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 8745.0

  Escenario: g7 - Evento de graduacion sin salon no esta permitido
    Dado que mi cuit personal es "20445556667"
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error
