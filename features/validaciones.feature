#language: es
@wip
Característica: Validaciones

  Escenario: v1 - Evento empresarial con menos de 20 personas no está permitido
    Dado que mi cuit personal es "30445556667"
    Dado un evento "empresarial" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 19 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: v2 - Casamiento contratado por un empresa no está permitido
    Dado que mi cuit personal es "30445556667"
    Dado un evento "casamiento" con servicio "normal"
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error

  Escenario: v3 - Fiesta de 15 contratada por una empresa no está permitido
    Dado que mi cuit personal es "30445556667"
    Dado un evento "fiesta de 15" con servicio "normal"
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error

  Escenario: v4 - Casamiento con mas de 300 personas no está permitido
    Dado que mi cuit personal es "27389877762"
    Dado un evento "casamiento" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 301 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: v5 - Casamiento con salon y mas de 150 personas
    Dado que mi cuit personal es "27389877762"
    Dado un evento "casamiento" con servicio "premium"
    Y contrata un salon
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 151 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: v6 - Evento empresarial sin cupo minimo
    Dado que mi cuit personal es "27389877762"
    Dado un evento "empresarial" con servicio "premium"
    Y que está programado para el "2021-05-09" que es un día no hábil
    Y que tendrá 4 comensales con menú "veggie"
    Y que tendrá 4 comensales con menú "mixto"
    Y que tendrá 2 comensales con menú "veggie"
    Cuando se presupuesta
    Entonces obtengo un error

  Escenario: v7 - Cuit no valido
    Dado que mi cuit personal es "40445556667"
    Dado un evento "casamiento" con servicio "normal"
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error
